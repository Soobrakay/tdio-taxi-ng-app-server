"""
taxi.middleware
===============
Provide middleware to authenticate users via tokens.
"""
from urllib.parse import parse_qs

from channels.auth import AuthMiddleware
from channels.db import database_sync_to_async
from channels.sessions import CookieMiddleware, SessionMiddleware
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.db import close_old_connections
from rest_framework_simplejwt.tokens import AccessToken

User = get_user_model()


@database_sync_to_async
def get_user(scope):
    """Get the user based on their token."""
    close_old_connections()
    query_string = parse_qs(scope["query_string"].decode())
    token = query_string.get("token")
    if not token:
        return AnonymousUser()

    try:
        access_token = AccessToken(token[0])
        user = User.objects.get(id=access_token["id"])
    # pylint: disable=broad-except
    except Exception:
        return AnonymousUser()
    if not user.is_active:
        return AnonymousUser()
    return user


class TokenAuthMiddleware(AuthMiddleware):
    """Set the user in the scope"""

    async def resolve_scope(self, scope):
        # pylint: disable=protected-access
        scope["user"]._wrapped = await get_user(scope)


def token_auth_middleware_stack(inner):
    """Authentication stack for the token."""
    return CookieMiddleware(SessionMiddleware(TokenAuthMiddleware(inner)))
