"""
trips.serializers
=================
Serializers for trips, users, and login.
"""
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from more_itertools import consume
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from trips.models import Trip


# pylint: disable=abstract-method
class LoginSerializer(TokenObtainPairSerializer):
    """Add a user's attributes to a token."""

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        user_data = UserSerializer(user).data
        filtered = filter(lambda kv: kv[0] != "id", user_data.items())
        # pylint: disable=unnecessary-dunder-call
        consume(token.__setitem__(kv[0], kv[1]) for kv in filtered)
        return token


class UserSerializer(serializers.ModelSerializer):
    """Serializer for validating users and passwords to create them."""

    class Meta:
        model = get_user_model()
        fields = (
            "id",
            "username",
            "password1",
            "password2",
            "first_name",
            "last_name",
            "group",
            "photo",
        )
        read_only_fields = ("id",)

    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    group = serializers.CharField()

    def validate(self, attrs):
        if attrs["password1"] != attrs["password2"]:
            raise serializers.ValidationError("Passwords must match.")
        return attrs

    def create(self, validated_data):
        group_data = validated_data.pop("group")
        group, _ = Group.objects.get_or_create(name=group_data)
        data = {
            key: value
            for key, value in validated_data.items()
            if key not in ("password1", "password2")
        }
        data["password"] = validated_data["password1"]
        user = self.Meta.model.objects.create_user(**data)
        user.groups.add(group)
        user.save()
        return user


class TripSerializer(serializers.ModelSerializer):
    """Flat trip serializer. Related objects are just their IDs."""

    class Meta:
        model = Trip
        fields = "__all__"
        read_only_fields = ("id", "created", "updated")


class NestedTripSerializer(serializers.ModelSerializer):
    """Trip serializer that expands related objects by 1 layer."""

    class Meta:
        model = Trip
        fields = "__all__"
        depth = 1
