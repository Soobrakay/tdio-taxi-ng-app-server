"""
trips.models
============
"""
import uuid

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.shortcuts import reverse


class User(AbstractUser):
    """User model to add .group property."""

    photo = models.ImageField(upload_to="photos", null=True, blank=True)

    @property
    def group(self):
        """Get the user's first Group if present."""
        groups = self.groups.all()
        return groups[0].name if groups else None


class Trip(models.Model):
    """Model for a trip from request to dropoff."""

    REQUESTED = "REQUESTED"
    STARTED = "STARTED"
    IN_PROGRESS = "IN_PROGRESS"
    COMPLETED = "COMPLETED"
    STATUSES = (
        (REQUESTED, REQUESTED),
        (STARTED, STARTED),
        (IN_PROGRESS, IN_PROGRESS),
        (COMPLETED, COMPLETED),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    pickup_address = models.CharField(max_length=255)
    dropoff_address = models.CharField(max_length=255)
    status = models.CharField(max_length=20, choices=STATUSES, default=REQUESTED)
    driver = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
        related_name="trips_as_driver",
    )
    rider = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
        related_name="trips_as_rider",
    )

    def __str__(self):
        return f"{self.id}"

    def get_absolute_url(self):
        """Get the URL back to this Trip."""
        return reverse("trip:trip_detail", kwargs={"trip_id": self.id})
