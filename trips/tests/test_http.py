import base64
import json
from io import BytesIO

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files.uploadedfile import SimpleUploadedFile
from PIL import Image
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from trips.models import Trip

PASSWORD = "pAssw0rd!"  # noqa


def create_user(username="user@example.com", password=PASSWORD, group_name="rider"):
    group, _ = Group.objects.get_or_create(name=group_name)
    user = get_user_model().objects.create_user(
        username=username, first_name="Test", last_name="User", password=password
    )
    user.groups.add(group)
    user.save()
    return user


def create_photo_file():
    data = BytesIO()
    Image.new("RGB", (100, 100)).save(data, "PNG")
    data.seek(0)
    return SimpleUploadedFile("photo.png", data.getvalue())


class AuthenticationTest(APITestCase):
    def test_user_can_sign_up(self):
        photo_file = create_photo_file()
        response = self.client.post(
            reverse("sign_up"),
            data={
                "username": "user@example.com",
                "first_name": "Test",
                "last_name": "User",
                "password1": PASSWORD,
                "password2": PASSWORD,
                "group": "rider",
                "photo": photo_file,
            },
        )
        user = get_user_model().objects.last()
        assert status.HTTP_201_CREATED == response.status_code
        assert response.data["id"] == user.id
        assert response.data["username"] == user.username
        assert response.data["first_name"] == user.first_name
        assert response.data["last_name"] == user.last_name
        assert response.data["group"] == user.group
        assert user.photo

    def test_user_can_log_in(self):
        user = create_user()
        response = self.client.post(
            reverse("login"), data={"username": user.username, "password": PASSWORD}
        )

        access = response.data["access"]
        header, payload, signature = access.split(".")
        decoded_payload = base64.b64decode(f"{payload}==")
        payload_data = json.loads(decoded_payload)

        assert status.HTTP_200_OK == response.status_code
        assert response.data["refresh"] is not None
        assert payload_data["id"] == user.id
        assert payload_data["username"] == user.username
        assert payload_data["first_name"] == user.first_name
        assert payload_data["last_name"] == user.last_name


class HttpTripTest(APITestCase):
    def setUp(self):
        self.user = create_user()
        self.client.login(username=self.user.username, password=PASSWORD)

    def test_user_can_list_trips(self):
        trips = [
            Trip.objects.create(
                pickup_address="A", dropoff_address="B", rider=self.user
            ),
            Trip.objects.create(
                pickup_address="B", dropoff_address="C", rider=self.user
            ),
            Trip.objects.create(pickup_address="C", dropoff_address="D"),
        ]
        response = self.client.get(reverse("trip:trip_list"))
        assert status.HTTP_200_OK == response.status_code
        expected = [str(trip.id) for trip in trips[0:2]]
        actual = [trip.get("id") for trip in response.data]
        assert sorted(expected) == sorted(actual)

    def test_user_can_retrieve_trip_by_id(self):
        trip = Trip.objects.create(
            pickup_address="A", dropoff_address="B", rider=self.user
        )
        response = self.client.get(trip.get_absolute_url())
        assert status.HTTP_200_OK == response.status_code
        assert str(trip.id) == response.data.get("id")
