"""
trips.views
===========
Views related to the trips app.
"""
from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import generics, permissions, viewsets
from rest_framework_simplejwt.views import TokenObtainPairView

from trips.models import Trip
from trips.serializers import LoginSerializer, NestedTripSerializer, UserSerializer


class SignUpView(generics.CreateAPIView):
    """Routes to register a user"""

    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer


class LoginView(TokenObtainPairView):
    """Routes to obtain a an authentication token via login."""

    serializer_class = LoginSerializer


class TripView(viewsets.ReadOnlyModelViewSet):
    """Routes to view Trip data"""

    lookup_field = "id"
    lookup_url_kwarg = "trip_id"
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = NestedTripSerializer

    def get_queryset(self):
        user = self.request.user
        if user.group == "driver":
            return Trip.objects.filter(Q(status=Trip.REQUESTED) | Q(driver=user))
        if user.group == "rider":
            return Trip.objects.filter(rider=user)
        return Trip.objects.none()
