POSTGRES_USER ?= taxi
POSTGRES_DB ?= taxi
POSTGRES_PASSWORD ?= taxi
POSTGRES_NAME ?= taxi-postgres

REDIS_NAME ?= taxi-redis


postgres:
	docker run --name $(POSTGRES_NAME) -p 5432:5432 \
		-e POSTGRES_USER=$(POSTGRES_USER) -e POSTGRES_DB=$(POSTGRES_DB) \
		-e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) -d postgres:alpine

postgres-start:
	docker start $(POSTGRES_NAME)

redis:
	docker run --name $(REDIS_NAME) -p 6379:6379 -d redis:alpine

redis-start:
	docker start $(REDIS_NAME)

start: postgres-start redis-start
	python manage.py runserver

test:
	pytest
